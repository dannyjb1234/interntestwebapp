Hello there,  

If you are an reading this you are likely an intern with one of the following names:  

Aaron  
Anna  
Claudia  
David  
Mardokai  
Scott  

We're gonna use this little app for a few reasons:  
- To get a short intro to SpringBoot REST endpoints  
-  How to run a SpringBoot app from the command line using maven  
-  Validate that everyone has access to the EC2 instance  

Assignment:  

1.) ssh into the server  
2.) go to /home/force/dev/InternTestWebApp/src/main/resources  
3.) edit application.properties which can be found in the resources folder by adding your  
first name in the 'names' property and your sid in the 'sids' property (separated by commas please)  
4.) go back to /home/force/dev/InternTestWebApp  
5.) use maven to recompile : 'mvn clean install'  
6.) use maven to run : mvn spring-boot:run  
7.) once it is running hit the endpoint http://172.20.19.118:8080/getName/[sid] in your browser  

If done correctly your name should be returned  

Once everyone is onboarded we'll go through rest endpoints as a group  